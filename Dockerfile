FROM registry.redhat.io/jboss-eap-7/eap73-openjdk8-openshift-rhel7:latest

## Pasamos variables de Entorno.
ENV JAVA_OPTS_APPEND="-Xms512m -Xmx512m"

ADD ./jboss-helloworld.war /opt/eap/standalone/deployments
